package com.example.apiservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.apiservice.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String> {

}
