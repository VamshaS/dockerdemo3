FROM openjdk:17-jdk-slim
EXPOSE 8080
COPY target/my-docker.jar my-docker.jar
ENTRYPOINT ["java","-jar","/my-docker.jar"]
